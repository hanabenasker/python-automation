import requests
import smtplib
import os
import paramiko
import linode_api4
import time
import schedule

EMAIL_ADDRESS = os.environ.get('EMAIL_ADDRESS')
EMAIL_PASSWORD = os.environ.get('EMAIL_PASSWORD')
LINODE_ADDRESS = os.environ.get('LINODE_ADDRESS')
LINODE_USERNAME = os.environ.get('LINODE_USERNAME')
PK_LOCATION = os.environ.get('PK_LOCATION')
LINODE_TOKEN = os.environ.get('LINODE_TOKEN')
INSTANCE_ID = os.environ.get('INSTANCE_ID')
URL = os.environ.get('URL')

def reboot_server_and_container():
    print('Rebooting the server...')
    client = linode_api4.LinodeClient(LINODE_TOKEN)
    nginx_server = client.load(linode_api4.Instance, INSTANCE_ID)
    nginx_server.reboot()

    while True:
        nginx_server = client.load(linode_api4.Instance, INSTANCE_ID)
        if nginx_server.status == 'running':
            time.sleep(5)
            restart_container()
            break


def send_notification(subject, email_body):
    print(f'Sending an email to {EMAIL_ADDRESS} ...')
    with smtplib.SMTP('smtp.gmail.com', 587) as smtp:
        smtp.starttls()
        smtp.ehlo()
        smtp.login(EMAIL_ADDRESS, EMAIL_PASSWORD)
        email = f'Subject: {subject}\n{email_body}'
        smtp.sendmail(EMAIL_ADDRESS, EMAIL_ADDRESS, email)


def restart_container():
    print('Restarting the application...')
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy)
    ssh.connect(LINODE_ADDRESS, username=LINODE_USERNAME, key_filename=PK_LOCATION)
    stdin, stdout, stderr = ssh.exec_command('docker start 7979bbb8ce6c')
    out = stdout.readlines()
    ssh.close()
    print('Application restarted')
    email = f'Application restarted successfully!'
    send_notification('[APPLICATION RESTARTED]', email)


def monitor_application():
    try:
        response = requests.get(URL)
        if response.status_code == 200:
            print("Application is running successfully!")
        else:
            print("Application down. Fix it!")
            message = f'Application returned {response.status_code} HTTP code. Fix the issue ASAP!'
            send_notification('SITE DOWN', message)
            restart_container()
    except Exception as ex:
        message = 'Application not accessible at all. Fix the issue ASAP!'
        send_notification('SITE DOWN', message)
        reboot_server_and_container()


schedule.every(5).seconds.do(monitor_application)

while True:
    schedule.run_pending()
