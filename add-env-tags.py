import boto3

ec2_client_paris = boto3.client('ec2', region_name="eu-west-3")
ec2_res_paris = boto3.resource('ec2', region_name="eu-west-3")

ins_ids_paris = []
reservations_paris = ec2_client_paris.describe_instances()['Reservations']

for res in reservations_paris:
    for instance in res['Instances']:
        ins_ids_paris.append(instance['InstanceId'])

response = ec2_res_paris.create_tags(
    Resources=ins_ids_paris,
    Tags=[
        {
            'Key': 'env',
            'Value': 'prod'
        },
    ]
)
