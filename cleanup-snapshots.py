import boto3
import schedule
from operator import itemgetter

ec2_client = boto3.client('ec2')


# Only leave the last 2 snapshots of a specific volume

def cleanup_snapshots():
    volumes = ec2_client.describe_volumes(
        Filters=[
            {
                'Name': 'tag:Name',
                'Values': ['prod']
            }
        ]
    )['Volumes']

    for volume in volumes:
        snapshots = ec2_client.describe_snapshots(
            Filters=[
                {
                    'Name': 'volume-id',
                    'Values': [volume['VolumeId']]
                }
            ],
            OwnerIds=['self']
        )['Snapshots']

        snapshots = sorted(snapshots, key=itemgetter('StartTime'), reverse=True)

        for snapshot in snapshots[2:]:
            print(f"Deleting napshot id {snapshot['SnapshotId']} started at {snapshot['StartTime']}")
            response = ec2_client.delete_snapshot(
                SnapshotId=snapshot['SnapshotId']
            )
            if response['ResponseMetadata']['HTTPStatusCode'] == 200:
                print(f"Snapshot {snapshot['SnapshotId']} deleted successfully !")


schedule.every().week.do(cleanup_snapshots)

while True:
    schedule.run_pending()
