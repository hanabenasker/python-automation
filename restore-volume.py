import boto3
from operator import itemgetter

ec2_client = boto3.client('ec2')
ec2_resource = boto3.resource('ec2')

# Creating a volume from a snapshot and attaching it to an existing instance

instance_id = "i-0b369699abc04e6ac"

instance_volume = ec2_client.describe_volumes(
    Filters=[
        {
            'Name': 'attachment.instance-id',
            'Values': [instance_id]
        }
    ]
)['Volumes'][0]  # we assume that it has only one volume

snapshots = ec2_client.describe_snapshots(
    Filters=[
        {
            'Name': 'volume-id',
            'Values': [instance_volume['VolumeId']]
        }
    ],
    OwnerIds=['self']
)['Snapshots']

latest_snapshot = sorted(snapshots, key=itemgetter('StartTime'), reverse=True)[0]

print(latest_snapshot['StartTime'])

new_volume = ec2_client.create_volume(
    SnapshotId=latest_snapshot['SnapshotId'],
    AvailabilityZone="eu-west-3c",
    TagSpecifications=[
        {
            'ResourceType': 'volume',
            'Tags': [
                {
                    'Key': 'Name',
                    'Value': 'prod'
                }
            ]
        }
    ]
)

while True:
    volume = ec2_resource.Volume(new_volume['VolumeId'])
    print(volume.state)
    if volume.state == 'available':
        ec2_resource.Instance(instance_id).attach_volume(
            Device="/dev/xvdb",
            VolumeId=new_volume['VolumeId']
        )
        break


