# Python Automation 

- **ec2-status-checks.py:** This Python script uses the Boto3 library to check the status of EC2 instances in the eu-central-1 region and prints the instance ID, state, instance status, and system status. It then schedules this check to run every 5 minutes using the schedule library.

- **eks-status-checks.py:** This Python script uses the Boto3 library to interact with the Amazon Elastic Kubernetes Service (EKS). It lists all EKS clusters in a specified region and prints out information for each cluster, including the cluster status, endpoint, and version

- **add-env-tags.py:** This Python script uses the Boto3 library to create tags with the key-value pair "env:prod" for all EC2 instances in the "eu-west-3" region. It first retrieves a list of instance IDs using the EC2 client, then uses the EC2 resource to create tags for each instance using the retrieved IDs.

- **cleanup-snapshots.py:** This Python script uses the Boto3 library to clean up old snapshots of an Amazon Elastic Block Store (EBS) volume. Specifically, it only keeps the last 2 snapshots of a specific volume tagged with "prod" and deletes any older snapshots. The script is designed to run on a schedule using the Schedule library.

- **monitor-website.py:** This Python script monitors a website hosted on a Linode instance and restarts the application container if it goes down. It sends email notifications about the status of the application and reboots the server and container in case of a catastrophic failure.

- **restore-volume.py:** This Python script creates a new EBS volume from the latest snapshot of an EC2 instance, tags it with 'Name: prod', and attaches it to the same instance as device '/dev/xvdb'.

- **volume-backups.py:** This script uses the Boto3 library to create snapshots of the volumes attached to production servers. It retrieves a list of volumes with a specific tag and creates snapshots for each volume. It also uses the schedule library to automate the creation of snapshots on a daily basis. The script runs continuously and checks for pending jobs to run.
